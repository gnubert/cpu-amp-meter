#include <stdint.h>

#include <avr/io.h>

#include "usb.h"

/***********************************************************************
 * configuration
 **********************************************************************/

#define AM  (B,2)

/***********************************************************************
 * public API declaration
 **********************************************************************/

enum cam_usb_commands {
    usbtiny_echo = 0,
    cam_set_val = 10,
};

/***********************************************************************
 * internal API declaration
 **********************************************************************/

void sys_init(void);

/***********************************************************************
 * internal API implementation
 **********************************************************************/

void sys_init(void)
{
    DDRB |= (1 << PB2);
    TCCR0A = (1 << COM0A1) | (1 << WGM00);
    OCR0A  = 0x00;

    TCCR0B = (1 << CS01);
}

/***********************************************************************
 * public API implementation
 **********************************************************************/

extern byte_t usb_setup(byte_t data[8])
{
    switch (data[1]) {
        case usbtiny_echo:
            return 8;

        case cam_set_val:
            OCR0A = data[2];
            return 0;
    }

    return 0;
}

/* Handle an IN packet. (USBTINY_CALLBACK_IN==1) */
extern byte_t usb_in(byte_t* data, byte_t len)
{
    return 0;
}

/* Handle an OUT packet. (USBTINY_CALLBACK_OUT==1) */
extern void usb_out(byte_t* data, byte_t len)
{
    return;
}

extern int main(void)
{
    sys_init();
    usb_init();

    while (1) {
        usb_poll();
    }
}
