#!/usr/bin/env python2

import sys, os.path
sys.path[0] = os.path.join(sys.path[0], '../usbtiny-1.7/util')
import usbtiny
import psutil

vendor  = 0x6666
product = 0x0001

dev = usbtiny.USBtiny(vendor, product)
while True:
    raw = psutil.cpu_percent(0.2)
    val = ((int)(raw/100 * 0xF7)) & 0x0000000000ff
    dev.control_in(10, val, 0, 8)
