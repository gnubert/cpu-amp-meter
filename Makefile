USBTINYVER ?= usbtiny-1.7

export USBTINY ?= $(PWD)/$(USBTINYVER)/usbtiny

.PHONY: all clean

all: $(USBTINYVER)/
	make -C src all

clean:
	make -C src clean

fuses:
	make -C src fuses

flash:
	make -C src flash

$(USBTINYVER): usbtiny-1.7.tar.gz
	tar xfz usbtiny-1.7.tar.gz

$(USBTINYVER).tar.gz:
	wget http://dicks.home.xs4all.nl/avr/usbtiny/$(USBTINYVER).tar.gz
